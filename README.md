# Himapan Webpage

## Description

Himapan is the main website of the Himapan company located in Germany, Bavaria

- It serves as the entry point for anyone interested in Himapan

## Installation

The site should run on nearly all current browser. Please let us know, if not and assistance is needed via [GitLab](https://gitlab.com/cyaton).

## Usage

Just like a normal web page

### Privacy

The site will inform visitors if it tries to collect any privacy related data from them

## Support

Feel free to contact us on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback, suggestions or collaboration attempts are welcome! Especially for other projects!

## Roadmap

Set up initial webpage to go online

## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Active development. Readme last updated on October 10th, 2023