import PropTypes from 'prop-types';
import styled from 'styled-components';
import { col } from '../styles/colors.js';
import {theme} from "../styles/theme.js";

const StyledBtn = styled.button`
  background-color: ${theme.colBackgr};
  color: ${props => props.$color};
  border: 2px solid ${props => props.$color};
  border-radius: 1.61rem;
  padding: 0.1rem 1.4rem;
  margin: 0.1rem;
  z-index: 400;
  
  &:hover {
    background-color: ${theme.colBackgrShadow};
    transition: background-color 1s;
  }
`;

const BtnCasing = styled.div`
  border: 0.5px solid #afafaf;
  border-radius: 1.71rem;
  display: inline-block;
  z-index: 395;
`;
export default function Btn({ onClick, children, color }) {
  return (
    <BtnCasing>
      <StyledBtn $color={color} onClick={onClick}>
        {children}
      </StyledBtn>
    </BtnCasing>
  );
}

Btn.propTypes = {
  children: PropTypes.object,
  onClick: PropTypes.func,
    color: PropTypes.string,
};
