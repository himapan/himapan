import {Trans} from "@lingui/macro";
import Btn from './Btn.jsx';
import {Link} from "react-router-dom";
import styled from "styled-components";
import {theme} from "../styles/theme.js";
import LanguageSwitcher from "./LanguageSwitcher.jsx";

const StyledFooter = styled.footer`
  margin-top: 2rem;
`;

const StyledUL = styled.ul`
  padding-left: 0;
  display: flex;
  justify-content: center;
`

const StyledLI = styled.li`
  font-size: 1rem;
  width: fit-content;
  margin: 1rem;
`

export default function Footer() {
  const goContact = () => {};

  return (
    <StyledFooter>
      <div>
        <div className="dflex justifyContentCenter">
          <Btn className="ma" onClick={goContact()}>
            <Trans>Contact</Trans>
          </Btn>
        </div>
        <StyledUL>
          <StyledLI><Link to="/provider"><Trans>Impressum</Trans></Link></StyledLI>
          <StyledLI><Trans>Rechtshinweise</Trans></StyledLI>
          <StyledLI><Trans>Datenschutz</Trans></StyledLI>
          <StyledLI><Trans>Cookies</Trans></StyledLI>
          <StyledLI><Trans>Verantwortung</Trans></StyledLI>
        </StyledUL>
      </div>
    </StyledFooter>
  );
}
