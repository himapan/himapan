import { loadCatalog } from '../utils/i18n.js';
import styled from 'styled-components';
import { col } from '../styles/colors.js';
import imgFlagDE from '../img/ui/flagDE200x200.2023.png';
import imgFlagEN from '../img/ui/flagUK200x200.2023.png';
import {theme} from "../styles/theme.js";

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  height: fit-content;
  width: fit-content;
  padding: 2px;
  margin: 0 1rem;
`
const StyledBtn = styled.button`
  width: fit-content;
  height: fit-content;
  border: none;
  background-color: ${theme.colBackgrT6};
`

const StyledImg = styled.img`
    display: block;
`

export default function LanguageSwitcher() {

  return (
    <StyledDiv>
      <StyledBtn style={{ marginRight: '10px' }} onClick={() => loadCatalog('de-DE')}>
        <StyledImg src={imgFlagDE} alt="Deutsch" height="30px" />
      </StyledBtn>
      <StyledBtn onClick={() => loadCatalog('en-US')}>
        <StyledImg src={imgFlagEN} alt="English" height="30px" />
      </StyledBtn>
    </StyledDiv>
  );
}
