import styled from 'styled-components';
import LanguageSwitcher from './LanguageSwitcher.jsx';
import { theme } from '../styles/theme.js';
import PropTypes from 'prop-types';

const StyledDivSidebar = styled.div`
  position: fixed;
  height: 100%;
  z-index: 900;
  width: 8rem;
  padding: 0;
  text-align: left;
  background: ${theme.colBackgrT6};
  transition: all 0.3s ease-in;
  backdrop-filter: blur(5px);
  visibility: ${(props) => (props.$sidebarIsOpen ?  'visible' : 'hidden')};
  @media (hover: hover) {
    /* when hover is supported → desktop */
    border-right: 0.2px solid ${theme.colFontDim8};
    left: ${(props) => (props.$sidebarIsOpen ?  '0' : '-8rem')};
    top: 1rem;
  }
  @media (hover: none) {
    /* when hover is not supported → touch*/
    border-left: 0.2px solid ${theme.colFontDim8};
    right: ${(props) => (props.$sidebarIsOpen ?  '0' : '-8rem')};
    bottom: 1rem;
  }
`;
const StyledLI = styled.li`
  margin-bottom: 1rem;
  list-style-type: none;
`;
const StyledUL = styled.ul`
  padding: 1rem;
  margin-top: 30px;
  
`;
export default function Sidebar({ sidebarIsOpen }) {
  return (
    <StyledDivSidebar $sidebarIsOpen={sidebarIsOpen} >
      <StyledUL aria-hidden={!sidebarIsOpen} id="main-menu">
        <StyledLI>
          <a href="#nowhere">Item 1</a>
        </StyledLI>
        <StyledLI>
          <a href="#nowhere">Item 2</a>
        </StyledLI>
        <StyledLI>
          <a href="#nowhere">Item 3</a>
        </StyledLI>
        <StyledLI>
          <LanguageSwitcher />
        </StyledLI>
      </StyledUL>
    </StyledDivSidebar>
  );
}

Sidebar.propTypes = {
  sidebarIsOpen: PropTypes.bool,
};
