import styled from 'styled-components';
import { col } from '../styles/colors.js';
import { theme } from '../styles/theme.js';

import { Trans } from '@lingui/macro';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const StyledHeader = styled.header`
  display: flex;
  background-color: ${theme.colBackgr};
  align-items: center;
  width: 8rem;
  height: 1.8rem;
  padding: 0;
  padding-left: 0.4rem;
  padding-right: 0.5rem;
  font-size: 1.3rem;
  font-weight: 500;
  font-family: 'Noto Sans Display', system-ui, sans-serif;
  filter: drop-shadow(0 0 10px ${theme.colFontT8});
`;

const StyledLink = styled(Link)`
  color: ${theme.colFont};
`;

const StyledHamburgerButton = styled.button`
  display: flex;
  margin-left: 0.5rem;
  border: none;
  background-color: rgba(0, 0, 0, 0);
  width: 30px;
  height: 30px;
  position: relative;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: 0.5s ease-in-out;
  -moz-transition: 0.5s ease-in-out;
  -o-transition: 0.5s ease-in-out;
  transition: 0.5s ease-in-out;
  cursor: pointer;
  &:focus {
    outline: none;
    border: none;
  }
`;

const StyledSpan = styled.span`
  display: block;
  position: absolute;
  height: 3px;
  width: 100%;
  background: ${theme.colFont};
  border-radius: 9px;
  opacity: 1;
  left: 0;
  -webkit-transform: rotate(0deg);
  -moz-transform: rotate(0deg);
  -o-transform: rotate(0deg);
  transform: rotate(0deg);
  -webkit-transition: 0.3s ease-in-out;
  -moz-transition: 0.3s ease-in-out;
  -o-transition: 0.3s ease-in-out;
  transition: 0.3s ease-in-out;

  &:nth-child(1) {
    top: ${props => props.$sidebarIsOpen ? "13px" : "6px"};
    -webkit-transform: ${props => props.$sidebarIsOpen ? "rotate(135deg)" : "rotate(0deg)"};
    -moz-transform: ${props => props.$sidebarIsOpen ? "rotate(135deg)" : "rotate(0deg)"};
    -o-transform: ${props => props.$sidebarIsOpen ? "rotate(135deg)" : "rotate(0deg)"};
    transform: ${props => props.$sidebarIsOpen ? "rotate(135deg)" : "rotate(0deg)"};
  }

  &:nth-child(2) {
    top: 13.5px;
    opacity: ${props => props.$sidebarIsOpen ? 0 : 1};
    left: ${props => props.$sidebarIsOpen ? "50px" : 0};
    transition: all 0.3s;
  }

  &:nth-child(3) {
    top: ${props => props.$sidebarIsOpen ? "13px" : "21px"};
    -webkit-transform: ${props => props.$sidebarIsOpen ? "rotate(-135deg)" : "rotate(0deg)"};
    -moz-transform: ${props => props.$sidebarIsOpen ? "rotate(-135deg)" : "rotate(0deg)"};
    -o-transform: ${props => props.$sidebarIsOpen ? "rotate(-135deg)" : "rotate(0deg)"};
    transform: ${props => props.$sidebarIsOpen ? "rotate(-135deg)" : "rotate(0deg)"};
  }
`;

export default function Header({ className, onToggle, sidebarIsOpen }) {
  const goContact = () => {};

  return (
    <div className={className}>
      <StyledHeader>
        <StyledLink to="/" aria-controls="main-menu">
          <Trans>HIMAPAN</Trans>
        </StyledLink>
        <StyledHamburgerButton onClick={onToggle} aria-expanded={sidebarIsOpen} aria-controls="main-menu">
          <StyledSpan $sidebarIsOpen={sidebarIsOpen} focusable="false"></StyledSpan>
          <StyledSpan $sidebarIsOpen={sidebarIsOpen} focusable="false"></StyledSpan>
          <StyledSpan $sidebarIsOpen={sidebarIsOpen} focusable="false"></StyledSpan>
        </StyledHamburgerButton>
      </StyledHeader>
    </div>
  );
}

Header.propTypes = {
  className: PropTypes.string,
  onToggle: PropTypes.func,
  sidebarIsOpen: PropTypes.bool,
};
