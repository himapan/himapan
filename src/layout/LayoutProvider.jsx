import PropTypes from 'prop-types';
import LayoutDefault from "./LayoutDefault.jsx";
import {useState} from "react";

export default function LayoutProvider({ children }) {
  const [layout, setLayout] = useState('LayoutDefault');

  return (
      <LayoutDefault>{children}</LayoutDefault>
  );
}

LayoutProvider.propTypes = {
  children: PropTypes.array,
};
