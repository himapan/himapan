import PropTypes from 'prop-types';
import Header from '../ui/Header.jsx';
import Footer from '../ui/Footer.jsx';
import styled, { keyframes, css } from 'styled-components';
import { theme } from '../styles/theme.js';
import Sidebar from '../ui/Sidebar.jsx';
import { useState } from 'react';

const StyledDiv = styled.div`
  min-height: 100vh;
  width: 100%;
  margin: 0;
  padding: 0;
  background-color: ${theme.colBackgr};
`;

const StyledHeader = styled(Header)`
  header {
    z-index: 1000;
    position: fixed;
    @media (hover: hover) {
      /* when hover is supported */
      top: 0;
      left: 0;
      border-radius: 0 0 12px 0;
      border-bottom: 0.2px ${theme.colFontDim8} solid;
      border-right: 0.2px ${theme.colFontDim8} solid;
    }
    @media (hover: none) {
      /* when hover is not supported → touch*/
      bottom: 0;
      right: 0;
      border-radius: 12px 0 0 0;
      border-top: 0.2px ${theme.colFontDim8} solid;
      border-left: 0.2px ${theme.colFontDim8} solid;
    }
  }
`;

const fadeOut = keyframes`
  from { opacity: 1; }
  to { opacity: 0.3; }
`;

const fadeIn = keyframes`
  from { opacity: 0.3; }
  to { opacity: 1; }
`;

const StyledWrapperDiv = styled.div`
  animation: ${(props) => (props.$sidebarIsOpen ? fadeOut : fadeIn)} 0.3s linear 1 normal;
  animation-fill-mode: forwards;
`;

const StyledContent = styled.div`
  padding: 5px;
  max-width: 800px;
  margin: auto;
  transition: all 0.3s ease-in;
`;

export default function LayoutDefault({ children }) {
  const [sidebarIsOpen, setSidebarIsOpen] = useState(false);

  function handleToggleSidebar(isOpen) {
    setSidebarIsOpen(isOpen);
  }
    return (
    <StyledDiv>
      <nav aria-label="Main menu" role="navigation">
        <StyledHeader onToggle={() => handleToggleSidebar(!sidebarIsOpen)} sidebarIsOpen={sidebarIsOpen} />
        <Sidebar sidebarIsOpen={sidebarIsOpen} />
      </nav>
      <StyledWrapperDiv $sidebarIsOpen={sidebarIsOpen}>
        <StyledContent>{children}</StyledContent>
        <Footer />
      </StyledWrapperDiv>
    </StyledDiv>
  );
}

LayoutDefault.propTypes = {
  children: PropTypes.array,
};
