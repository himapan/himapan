import {createGlobalStyle} from "styled-components";
import {theme} from "./theme.js";
import {col} from "./colors.js";

export const GlobalStyles = createGlobalStyle`

  // === General

  *,
  *::before,
  *::after {
    box-sizing: border-box;
    padding: 0;
    margin: 0;

    // Animations for dark mode
    transition: background-color 0.3s, border 0.3s;
  }

  html {
    font-size: 16px;
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    -webkit-text-size-adjust: 100%;
    -webkit-tap-highlight-color: transparent;
  }

  @media screen and (min-width: 320px) {
    html {
      font-size: calc(16px + 6 * ((100vw - 320px) / 680));
    }
  }

  @media screen and (min-width: 1000px) {
    html {
      font-size: 22px;
    }
  }

  body {
    font-family: "Noto Sans", system-ui, sans-serif;
    font-weight: 400;
    color: ${theme.colFont};
    background-color: ${theme.colBackgr};
    transition: color 0.3s, background-color 0.3s;
    height: auto;
    min-height: 100vh;
    width: 100%;
    margin: 0;
    padding: 0;
    text-align: left;
    line-height: 1.5;
  }

  // Important for fixed menu
  #root {
    overflow: auto;
  }
  
  // === Text Elements
  p,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    overflow-wrap: break-word;
    hyphens: auto;
  }

  h1 {
    font-family: "Noto Sans Display", system-ui, sans-serif;
    font-weight: 100;
    text-transform: uppercase;
    text-align: center;
    line-height: 1.25;
    letter-spacing: 0.14rem;
    padding: 0 25px;
    margin-top: 2rem;
    margin-bottom: 1rem;
  }

  h2 {
    font-weight: 400;
    font-family: "Noto Sans Display", system-ui, sans-serif;
    font-size: 1.8rem;
    letter-spacing: 1px;
    margin-top: 3rem;
  }
  
  p {
    margin-bottom: 1rem;
    line-height: 1.5;
  }

  p:last-child {
    margin-bottom: 0px;
  }

  a {
    color: ${theme.colAction};
    text-decoration: none;
  }

  ul {
    list-style: none;
  }

  input,
  button,
  textarea,
  select {
    font: inherit;
    color: inherit;
  }

  button {
    cursor: pointer;
  }

  button:focus {
    border-radius: 5rem;
  }

  *:disabled {
    cursor: not-allowed;
  }

  select:disabled,
  input:disabled {
    background-color: ${col.white.d2};
    color: ${col.black.l12};
  }

  input:focus,
  button:focus,
  textarea:focus,
  select:focus {
    outline: 2px solid ${theme.colMain};
    outline-offset: -1px;
  }

  /* Parent selector, finally 😃 */
  button:has(svg) {
    line-height: 0;
  }

  img {
    max-width: 100%;
    /* For dark mode */
    filter: grayscale(var(0)) opacity(var(100%));
  }

  //=== Spacing

  .ma { margin: auto; }

  .mt00 { margin-top: 0; }
  .mt10 { margin-top: 1rem; }
  .mt20 { margin-top: 2rem; }
  .mt30 { margin-top: 3rem; }

  // === Display

  .dblock { display: block; }

  .dflex { display: flex; }

  .justifyContentCenter { justify-content: center; }

  // === Text

  .notoSansDisplay {
    font-family: "Noto Sans Display", system-ui;
  }

  .tUppercase { text-transform: uppercase; }

  .weight100 { font-weight: 100; }

  .fontsize18 { font-size: 1.8rem; }
`
