// === Helper functions ===

// Generate rgba string
const getRGBa = (r, g, b, a) => {
  return `rgba(${r}, ${g}, ${b}, ${a})`;
};

// Check if a given color value is mutable while still generating a valid rgba value
const colMutable = (r, g, b, a, valRGB, valA) => {
  if (
    r + valRGB <= 255 &&
    r + valRGB >= 0 &&
    g + valRGB <= 255 &&
    g + valRGB >= 0 &&
    b + valRGB <= 255 &&
    b + valRGB >= 0 &&
    a + valA <= 1 &&
    a + valA >= 0
  ) {
    return true;
  } else {
    return false;
  }
};

// Changes the color value evenly on r,g,b and/or transparency valA by given values
const mutateRGBa = (r, g, b, a, valRGB, valA) => {
  if (colMutable(r, g, b, a, valRGB, valA)) {
    return getRGBa(r + valRGB, g + valRGB, b + valRGB, a + valA);
  } else {
    return getRGBa(r, g, b, a);
  }
};

// === System color template ===

// Defines a system color with all subgradients of this color
function color(r, g, b, a, hex, name) {
  this.r = r;
  this.g = g;
  this.b = b;
  this.a = 1;
  this.hex = hex;
  this.name = name;
  this.rgb = `rgb(${this.r}, ${this.g}, ${this.b})`;
  this.rgba = `rgba(${this.r}, ${this.g}, ${this.b}, 1)`;

  // Main color gradients in transparency
  this.t2 = getRGBa(this.r, this.g, this.b, 0.8);
  this.t4 = getRGBa(this.r, this.g, this.b, 0.6);
  this.t6 = getRGBa(this.r, this.g, this.b, 0.4);
  this.t8 = getRGBa(this.r, this.g, this.b, 0.2);

  // Gradients generator:
  // direction number 1 makes lighter gradients in steps by 10 (rgba)
  // direction number -1 makes darker gradients
  const gradientGenerator = (directionNumber) => {
    for (let i = 1; i < 25; i++) {
      let val = directionNumber * i * 10;
      if (mutateRGBa(this.r, this.g, this.b, 1, val, 0) === getRGBa(this.r, this.g, this.b, this.a)) {
        break;
      } else {
        let prefix;
        directionNumber === 1 ? prefix = 'l' : prefix = 'd';
        this[prefix + i] = mutateRGBa(this.r, this.g, this.b, 1, val, 0);
      }
    }
  };

  // Generates color gradients in darkness d1, d2,..., d9
  // Eg col.main.d3 means three tones darker
  gradientGenerator(-1);

  // Main color gradients in lightness
  // Eg col.main.l3 means three tones lighter
  gradientGenerator(1);
}

// === System colors ===

function himapanColors() {

  // Soft White
  this.white = new color(245, 245, 245, 1, '#F5F5F5', 'Soft white');

  // Soft Black
  this.black = new color(5, 5, 5, 1, '#232323FF', 'Soft black');

  // Company's main color, C2A
  this.main = new color(96, 116, 58, 1, '#60743A', 'Himapan green');

  // Accent
  this.triadicBlue = new color(58, 96, 116, 1, '#3A6074', 'Main complementary 1');

  // Functionality like links, buttons
  this.triadicPurple = new color(116, 58, 96, 1, '#743A60', 'Main complementary 2');

  // Special and rare heavy accent
  this.complement = new color(78, 58, 116, 1, '#4e3A74', 'Main complementary 3');

  // Feedback colors
  this.success = new color(90, 165, 139, 1, '#5AA58B', 'Success');
  this.warning = new color(194, 50, 111, 1, '#C2326F', 'Warning');
  this.info = new color(238, 184, 66, 1, '#EEB842', 'Info');
}

export const col = new himapanColors();
