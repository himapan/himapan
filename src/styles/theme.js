import {col} from "./colors.js";

function setTheme() {
    const boolLightMode = !window.matchMedia('(prefers-color-scheme: dark)').matches;
    this.light = boolLightMode;
    this.colBackgr = boolLightMode ? col.white.hex : col.black.hex;
    this.colBackgrT6 = boolLightMode ? col.white.t6 : col.black.t6;
    this.colBackgrAccent = boolLightMode ? col.white.l1 : col.black.d1;
    this.colBackgrShadow = boolLightMode ? col.white.d2 : col.black.l2;
    this.colFont = boolLightMode ? col.black.hex : col.white.hex;
    this.colFontDim8 = boolLightMode ? col.black.l8 : col.white.d8;
    this.colFontT8 = boolLightMode ? col.black.t8 : col.white.t8;
    this.colFontInvert = boolLightMode ? col.white.hex : col.black.hex;
    this.colMain = boolLightMode ? col.main.hex : col.main.l7;
    this.colAction = boolLightMode ? col.triadicPurple.hex : col.triadicPurple.l7;
}

export const theme = new setTheme();