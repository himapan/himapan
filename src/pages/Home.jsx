import { Trans } from '@lingui/macro';
import logo from '../img/logo/logo.png';
import LayoutProvider from "../layout/LayoutProvider.jsx";

export default function Home() {
  return (
    <LayoutProvider>
      <img className="ma dblock mt10" height="400px" src={logo} alt="logo Himapan" />
      <p className="mt00 fontsize18 tUppercase weight100 notoSansDisplay" style={{ textAlign: 'center' }}>
        <Trans>Be the change</Trans>
      </p>
    </LayoutProvider>
  );
}
