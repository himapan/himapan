import { Trans } from '@lingui/macro';
import LayoutMobile from '../layout/LayoutMobile.jsx';
import LayoutProvider from '../layout/LayoutProvider.jsx';

export default function Provider() {
  return (
    <LayoutProvider>
      <h1>
        <Trans>Impressum</Trans>
      </h1>

      <h2>
        <Trans>Angaben gem&auml;&szlig; &sect; 5 TMG</Trans>
      </h2>
      <p>
        <Trans>
          Philipp Montazem
          <br />
          Dienstleistungen und Handel f&uuml;r Gesundheit und Umwelt
          <br />
          Keltenstr. 12
          <br />
          86477 Adelsried
        </Trans>
      </p>

      <h2>
        <Trans>Kontakt</Trans>
      </h2>
      <p>
        <Trans>
          Telefon: +49 160 2822 466
          <br />
          E-Mail: mail@himapan.org
        </Trans>
      </p>

      <h2>
        <Trans>Redaktionell verantwortlich</Trans>
      </h2>
      <p>
        <Trans>
          Philipp Montazem
          <br />
          Dr.-Troeltsch-Str. 1<br />
          86179 Augsburg
        </Trans>
      </p>

      <h2>
        <Trans>EU-Streitschlichtung</Trans>
      </h2>
      <p>
        <Trans>
          Die Europ&auml;ische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit:{' '}
          <a href="https://ec.europa.eu/consumers/odr/" target="_blank" rel="noopener noreferrer external">
            https://ec.europa.eu/consumers/odr/
          </a>
          .<br /> Unsere E-Mail-Adresse finden Sie oben im Impressum.
        </Trans>
      </p>

      <h2>
        <Trans>Verbraucher&shy;streit&shy;beilegung/Universal&shy;schlichtungs&shy;stelle</Trans>
      </h2>
      <p>
        <Trans>
          Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
          teilzunehmen.
        </Trans>
      </p>
    </LayoutProvider>
  );
}
