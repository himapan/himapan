import { useState } from 'react';
import { Trans } from '@lingui/macro';

import './styles/pre.scss';

import LayoutMobile from './layout/LayoutMobile.jsx';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Provider from './pages/Provider.jsx';
import Home from './pages/Home.jsx';
import { GlobalStyles } from './styles/GlobalStyles.js';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />,
  },
  {
    path: '/provider',
    element: <Provider />,
  },
]);

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <GlobalStyles />
        <RouterProvider router={router} />
    </>
  );
}

export default App;
