import React, { useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import { loadCatalog } from './utils/i18n.js';
import { I18nProvider } from '@lingui/react';

import App from './App.jsx';

import { i18n } from '@lingui/core';
import Home from "./pages/Home.jsx";
import styled from "styled-components";

await loadCatalog('en');

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <I18nProvider i18n={i18n}>
        <App/>
    </I18nProvider>
  </React.StrictMode>,
);
