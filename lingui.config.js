const config = {
    locales: ["en-US", "de-DE"],
    catalogs: [
        {
            path: "src/locales/{locale}",
            include: ["src"],
        },
    ],
};

export default config;